// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.5 (swiftlang-1300.0.31.1 clang-1300.0.29.1)
// swift-module-flags: -target arm64-apple-ios8.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name MarqueeLabel
import QuartzCore
import Swift
import UIKit
import _Concurrency
@objc @_inheritsConvenienceInitializers @IBDesignable @_Concurrency.MainActor(unsafe) open class MarqueeLabel : UIKit.UILabel, QuartzCore.CAAnimationDelegate {
  public enum MarqueeType : Swift.CaseIterable {
    case left
    case leftRight
    case right
    case rightLeft
    case continuous
    case continuousReverse
    public static func == (a: MarqueeLabel.MarqueeLabel.MarqueeType, b: MarqueeLabel.MarqueeLabel.MarqueeType) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public typealias AllCases = [MarqueeLabel.MarqueeLabel.MarqueeType]
    public static var allCases: [MarqueeLabel.MarqueeLabel.MarqueeType] {
      get
    }
    public var hashValue: Swift.Int {
      get
    }
  }
  @_Concurrency.MainActor(unsafe) open var type: MarqueeLabel.MarqueeLabel.MarqueeType {
    get
    set
  }
  @_Concurrency.MainActor(unsafe) open var scrollSequence: [MarqueeLabel.MarqueeStep]?
  @_Concurrency.MainActor(unsafe) open var animationCurve: UIKit.UIView.AnimationCurve
  @objc @IBInspectable @_Concurrency.MainActor(unsafe) open var labelize: Swift.Bool {
    @objc get
    @objc set
  }
  @objc @IBInspectable @_Concurrency.MainActor(unsafe) open var holdScrolling: Swift.Bool {
    @objc get
    @objc set
  }
  @objc @IBInspectable @_Concurrency.MainActor(unsafe) public var forceScrolling: Swift.Bool {
    @objc get
    @objc set
  }
  @objc @IBInspectable @_Concurrency.MainActor(unsafe) open var tapToScroll: Swift.Bool {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) open var isPaused: Swift.Bool {
    get
  }
  @_Concurrency.MainActor(unsafe) open var awayFromHome: Swift.Bool {
    get
  }
  @_Concurrency.MainActor(unsafe) open var animationPosition: CoreGraphics.CGFloat? {
    get
  }
  public enum SpeedLimit {
    case rate(CoreGraphics.CGFloat)
    case duration(CoreGraphics.CGFloat)
  }
  @_Concurrency.MainActor(unsafe) open var speed: MarqueeLabel.MarqueeLabel.SpeedLimit {
    get
    set
  }
  @objc @available(*, deprecated, message: "Use speed property instead")
  @IBInspectable @_Concurrency.MainActor(unsafe) open var scrollDuration: CoreGraphics.CGFloat {
    @objc get
    @objc set
  }
  @objc @available(*, deprecated, message: "Use speed property instead")
  @IBInspectable @_Concurrency.MainActor(unsafe) open var scrollRate: CoreGraphics.CGFloat {
    @objc get
    @objc set
  }
  @objc @IBInspectable @_Concurrency.MainActor(unsafe) open var leadingBuffer: CoreGraphics.CGFloat {
    @objc get
    @objc set
  }
  @objc @IBInspectable @_Concurrency.MainActor(unsafe) open var trailingBuffer: CoreGraphics.CGFloat {
    @objc get
    @objc set
  }
  @objc @IBInspectable @_Concurrency.MainActor(unsafe) open var fadeLength: CoreGraphics.CGFloat {
    @objc get
    @objc set
  }
  @objc @IBInspectable @_Concurrency.MainActor(unsafe) open var animationDelay: CoreGraphics.CGFloat
  @_Concurrency.MainActor(unsafe) public var animationDuration: CoreGraphics.CGFloat {
    get
  }
  @_Concurrency.MainActor(unsafe) open class func restartLabelsOfController(_ controller: UIKit.UIViewController)
  @_Concurrency.MainActor(unsafe) open class func controllerViewWillAppear(_ controller: UIKit.UIViewController)
  @_Concurrency.MainActor(unsafe) open class func controllerViewDidAppear(_ controller: UIKit.UIViewController)
  @_Concurrency.MainActor(unsafe) open class func controllerLabelsLabelize(_ controller: UIKit.UIViewController)
  @_Concurrency.MainActor(unsafe) open class func controllerLabelsAnimate(_ controller: UIKit.UIViewController)
  @_Concurrency.MainActor(unsafe) public init(frame: CoreGraphics.CGRect, rate: CoreGraphics.CGFloat, fadeLength fade: CoreGraphics.CGFloat)
  @_Concurrency.MainActor(unsafe) public init(frame: CoreGraphics.CGRect, duration: CoreGraphics.CGFloat, fadeLength fade: CoreGraphics.CGFloat)
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @_Concurrency.MainActor(unsafe) @objc convenience override dynamic public init(frame: CoreGraphics.CGRect)
  @objc override dynamic open func awakeFromNib()
  @available(iOS 8.0, *)
  @objc override dynamic open func prepareForInterfaceBuilder()
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func layoutSubviews()
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func willMove(toWindow newWindow: UIKit.UIWindow?)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func didMoveToWindow()
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func sizeThatFits(_ size: CoreGraphics.CGSize) -> CoreGraphics.CGSize
  @_Concurrency.MainActor(unsafe) open func sizeThatFits(_ size: CoreGraphics.CGSize, withBuffers: Swift.Bool) -> CoreGraphics.CGSize
  @_Concurrency.MainActor(unsafe) open func textLayoutSize() -> CoreGraphics.CGSize
  @_Concurrency.MainActor(unsafe) open func labelShouldScroll() -> Swift.Bool
  @_Concurrency.MainActor(unsafe) @objc public func animationDidStop(_ anim: QuartzCore.CAAnimation, finished flag: Swift.Bool)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open class var layerClass: Swift.AnyClass {
    @_Concurrency.MainActor(unsafe) @objc get
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func draw(_ layer: QuartzCore.CALayer, in ctx: CoreGraphics.CGContext)
  @objc @_Concurrency.MainActor(unsafe) public func restartForViewController(_ notification: Foundation.Notification)
  @objc @_Concurrency.MainActor(unsafe) public func labelizeForController(_ notification: Foundation.Notification)
  @objc @_Concurrency.MainActor(unsafe) public func animateForController(_ notification: Foundation.Notification)
  @_Concurrency.MainActor(unsafe) public func triggerScrollStart()
  @objc @_Concurrency.MainActor(unsafe) public func restartLabel()
  @available(*, deprecated, message: "Use the shutdownLabel function instead")
  @_Concurrency.MainActor(unsafe) public func resetLabel()
  @objc @_Concurrency.MainActor(unsafe) public func shutdownLabel()
  @_Concurrency.MainActor(unsafe) public func pauseLabel()
  @_Concurrency.MainActor(unsafe) public func unpauseLabel()
  @objc @_Concurrency.MainActor(unsafe) public func labelWasTapped(_ recognizer: UIKit.UIGestureRecognizer)
  @_Concurrency.MainActor(unsafe) open func textCoordinateForFramePoint(_ point: CoreGraphics.CGPoint) -> CoreGraphics.CGPoint?
  @_Concurrency.MainActor(unsafe) open func labelWillBeginScroll()
  @_Concurrency.MainActor(unsafe) open func labelReturnedToHome(_ finished: Swift.Bool)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func forBaselineLayout() -> UIKit.UIView
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var forLastBaselineLayout: UIKit.UIView {
    @_Concurrency.MainActor(unsafe) @objc get
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var text: Swift.String? {
    @_Concurrency.MainActor(unsafe) @objc get
    @_Concurrency.MainActor(unsafe) @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var attributedText: Foundation.NSAttributedString? {
    @_Concurrency.MainActor(unsafe) @objc get
    @_Concurrency.MainActor(unsafe) @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var font: UIKit.UIFont! {
    @_Concurrency.MainActor(unsafe) @objc get
    @_Concurrency.MainActor(unsafe) @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var textColor: UIKit.UIColor! {
    @_Concurrency.MainActor(unsafe) @objc get
    @_Concurrency.MainActor(unsafe) @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var backgroundColor: UIKit.UIColor? {
    @_Concurrency.MainActor(unsafe) @objc get
    @_Concurrency.MainActor(unsafe) @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var shadowColor: UIKit.UIColor? {
    @_Concurrency.MainActor(unsafe) @objc get
    @_Concurrency.MainActor(unsafe) @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var shadowOffset: CoreGraphics.CGSize {
    @_Concurrency.MainActor(unsafe) @objc get
    @_Concurrency.MainActor(unsafe) @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var highlightedTextColor: UIKit.UIColor? {
    @_Concurrency.MainActor(unsafe) @objc get
    @_Concurrency.MainActor(unsafe) @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var isHighlighted: Swift.Bool {
    @_Concurrency.MainActor(unsafe) @objc get
    @_Concurrency.MainActor(unsafe) @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var isEnabled: Swift.Bool {
    @_Concurrency.MainActor(unsafe) @objc get
    @_Concurrency.MainActor(unsafe) @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var numberOfLines: Swift.Int {
    @_Concurrency.MainActor(unsafe) @objc get
    @_Concurrency.MainActor(unsafe) @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var baselineAdjustment: UIKit.UIBaselineAdjustment {
    @_Concurrency.MainActor(unsafe) @objc get
    @_Concurrency.MainActor(unsafe) @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var intrinsicContentSize: CoreGraphics.CGSize {
    @_Concurrency.MainActor(unsafe) @objc get
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var tintColor: UIKit.UIColor! {
    @_Concurrency.MainActor(unsafe) @objc get
    @_Concurrency.MainActor(unsafe) @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func tintColorDidChange()
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var contentMode: UIKit.UIView.ContentMode {
    @_Concurrency.MainActor(unsafe) @objc get
    @_Concurrency.MainActor(unsafe) @objc set
  }
  @objc override dynamic open var isAccessibilityElement: Swift.Bool {
    @objc get
    @objc set
  }
  @objc deinit
}
public protocol MarqueeStep {
  var timeStep: CoreGraphics.CGFloat { get }
  var timingFunction: UIKit.UIView.AnimationCurve { get }
  var edgeFades: MarqueeLabel.EdgeFade { get }
}
public struct ScrollStep : MarqueeLabel.MarqueeStep {
  public enum Position {
    case home
    case away
    case partial(CoreGraphics.CGFloat)
  }
  public let timeStep: CoreGraphics.CGFloat
  public let timingFunction: UIKit.UIView.AnimationCurve
  public let position: MarqueeLabel.ScrollStep.Position
  public let edgeFades: MarqueeLabel.EdgeFade
  public init(timeStep: CoreGraphics.CGFloat, timingFunction: UIKit.UIView.AnimationCurve = .linear, position: MarqueeLabel.ScrollStep.Position, edgeFades: MarqueeLabel.EdgeFade)
}
public struct FadeStep : MarqueeLabel.MarqueeStep {
  public let timeStep: CoreGraphics.CGFloat
  public let timingFunction: UIKit.UIView.AnimationCurve
  public let edgeFades: MarqueeLabel.EdgeFade
  public init(timeStep: CoreGraphics.CGFloat, timingFunction: UIKit.UIView.AnimationCurve = .linear, edgeFades: MarqueeLabel.EdgeFade)
}
public struct EdgeFade : Swift.OptionSet {
  public let rawValue: Swift.Int
  public static let leading: MarqueeLabel.EdgeFade
  public static let trailing: MarqueeLabel.EdgeFade
  public init(rawValue: Swift.Int)
  public typealias ArrayLiteralElement = MarqueeLabel.EdgeFade
  public typealias Element = MarqueeLabel.EdgeFade
  public typealias RawValue = Swift.Int
}
extension MarqueeLabel.MarqueeLabel.MarqueeType : Swift.Equatable {}
extension MarqueeLabel.MarqueeLabel.MarqueeType : Swift.Hashable {}
