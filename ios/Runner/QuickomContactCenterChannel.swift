//
//  FlutterMethodChannelManager.swift
//  Runner
//
//  Created by Thai Tran on 4/2/21.
//

import Foundation
import CCSDK


class QuickomContactCenterChannel : NSObject, CallCenterSDKProtocol {
    
    public static let fmcMethodNameStart = "start"
    public static let fmcMethodNameForceRequestCall = "force_request_call"
    public static let fmcForceRequestCallReturnResult = "force_request_call_return_result"
    public static let fmcForceRequestCallReceiveInformation = "force_request_call_receive_info"
    public static let fmcMethodNameLogin = "login"
    public static let fmcMethodNameLogout = "logout"
    public static let fmcMethodNameUpdatePushToken = "update_push_token"
    public static let fmcMethodNameHandlePushMessage = "handle_push_message"
    
    public static let fmcMethodNameCommonReturnResult = "common_callback_result"


    
    public static let shared = QuickomContactCenterChannel()
    
    private var channel : FlutterMethodChannel? = nil
    
    private var enterpriseName : String? = nil
    var pushkitToken : String? = nil
    var normalToken : String? = nil

    
    override init() {
        super.init()
        
    }
    
    
    func registerFlutterChannels(controller : FlutterViewController) {
        self.channel = FlutterMethodChannel(name: "com.quickom.sdk.contact_center",
                                       binaryMessenger: controller.binaryMessenger)
        self.channel?.setMethodCallHandler( { (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
            
            if (call.method == QuickomContactCenterChannel.fmcMethodNameStart) {
                if let args = call.arguments as? [String : Any] {
                    if let name = args["enterpriseName"] as? String {
                        self.enterpriseName = name
                    }
                }
                
                CallCenterSDK.start()
                CallCenterSDK.setCCSDKDelegate(self)
            } else if (call.method == QuickomContactCenterChannel.fmcMethodNameForceRequestCall) {
                if let args = call.arguments as? [String : Any] {
                    if let url = args["url"] as? String {
                        let userId = args["userId"] as? String
                        let name = args["name"] as? String
                        let email = args["email"] as? String
                        let phone = args["phone"] as? String
                        let claimCode = args["claimCode"] as? String

                        CallCenterSDK.startCallCallCenter(with: url, userID: userId, name: name, email: email, phone: phone, claimCode: claimCode, callOption: NORMAL, completion: { (info) in
                            if let ainfo = info {
                                if let errorCode = ainfo["error"] as? String, errorCode.count > 0 {
                                    DispatchQueue.main.async { [weak self] in
                                        guard let `self` = self else { return }
                                        
                                        let errorMessage = (ainfo["errorMessage"] as? String) ?? ""
                                        let r = ["success" : 0, "errorCode" : errorCode, "errorDescription" : errorMessage]
                                            as [String : Any]
                                        self.invokeFlutterMethod(name: QuickomContactCenterChannel.fmcForceRequestCallReturnResult, args: r, resultBlock: {(_) in })
                                    }
                                } else {
                                    DispatchQueue.main.async { [weak self] in
                                        guard let `self` = self else { return }
                                        
                                        let r = ["success" : 1]
                                        self.invokeFlutterMethod(name: QuickomContactCenterChannel.fmcForceRequestCallReturnResult, args: r, resultBlock: {(_) in })
                                    }
                                }
                            }
                        })
                    } else {
                        result(["error" : "Can not find argument: url"])
                    }
                }
            } else if (call.method == QuickomContactCenterChannel.fmcMethodNameLogin) {
                if let args = call.arguments as? [String : Any] {
                    if let username = args["username"] as? String, let pass = args["password"] as? String {
                        CallCenterSDK.loginSDK(withUser: username, password: pass, completion: { (result) in
                            DispatchQueue.main.async { [weak self] in
                                guard let `self` = self else { return }
                                
                                if let token = self.pushkitToken, let deviceId = BPXLUUIDHandler.uuid() {
                                    CallCenterSDK.updatePushToken(withDeviceId: deviceId,
                                                                  voipToken: token,
                                                                  normalToken: self.normalToken,
                                                                  enterprise: self.enterpriseName) { result in
                                        
                                    }
                                }
                                
                                
//                                let r = ["success" : 1]
                                self.invokeFlutterMethod(name: QuickomContactCenterChannel.fmcMethodNameCommonReturnResult, args: result, resultBlock: {(_) in
                                    
                                })
                            }
                        })
                    } else {
                        result(["error" : "Can not find argument: username and password for login feature"])
                    }
                }
            } else if (call.method == QuickomContactCenterChannel.fmcMethodNameLogout) {
                CallCenterSDK.logoutSDK()
            }
//            else if (call.method == QuickomContactCenterChannel.fmcMethodNameUpdatePushToken) {
//                if let args = call.arguments as? [String : Any] {
//                    if let entepriseName = args["enterpriseName"] as? String,
//                       let deviceId = args["deviceId"] as? String,
//                       let token = args["pushToken"] as? String {
//                        CallCenterSDK.updatePushToken(withDeviceId: deviceId, voipToken: token, normalToken: nil, enterprise: entepriseName, completion: {(result) in
//                            DispatchQueue.main.async { [weak self] in
//                                guard let `self` = self else { return }
//
////                                let r = ["success" : 1]
//                                self.invokeFlutterMethod(name: QuickomContactCenterChannel.fmcMethodNameCommonReturnResult, args: result, resultBlock: {(_) in
//
//                                })
//                            }
//                        })
//                    } else {
//                        result(["error" : "Can not find argument: deviceId, token, entepriseName for update push token feature"])
//                    }
//                }
//            }
//            else if (call.method == QuickomContactCenterChannel.fmcMethodNameHandlePushMessage) {
//                if let args = call.arguments as? [String : Any] {
//                    if let deviceId = args["deviceId"] as? String,
//                       let data = args["data"] as? [String : String] {
//                        CallCenterSDK.handlePushkitPayload(<#T##payload: String##String#>)
//                    } else {
//                        result(["error" : "Can not find argument: deviceId, token, entepriseName for update push token feature"])
//                    }
//                }
//            }
        })
    }
    
    func invokeFlutterMethod(name : String, args : [AnyHashable : Any]? = nil, resultBlock : @escaping (Any?) -> (Void)) {
        self.channel?.invokeMethod(name, arguments: args) { (result) in
            resultBlock(result)
        }
    }
    
    func callCenterSDK_didMakeCall(withInfo info: [String : Any]) {
        if let code = info["code"] as? String, code.count > 0 {
//            let email = (info["email"] as? String) ?? ""
//            let phone = (info["phone"] as? String) ?? ""
            
            self.invokeFlutterMethod(name: QuickomContactCenterChannel.fmcForceRequestCallReceiveInformation,
                                     args: info,
                                     resultBlock: {(_) in })
        }
    }
    
    func callCenterSDK_didMakeChat(withInfo info: [String : Any]) {
        if let code = info["code"] as? String, code.count > 0 {
//            let email = (info["email"] as? String) ?? ""
//            let phone = (info["phone"] as? String) ?? ""
            
            self.invokeFlutterMethod(name: QuickomContactCenterChannel.fmcForceRequestCallReceiveInformation,
                                     args: info,
                                     resultBlock: {(_) in })
        }
    }
    
    func handleReceivePushToken(pushkitToken pToken : String?, normalToken nToken : String?) {
        if CallCenterSDK.isSdkLoggedIn() {
            if let deviceId = BPXLUUIDHandler.uuid() {
                CallCenterSDK.updatePushToken(withDeviceId: deviceId, voipToken: pToken, normalToken: nToken, enterprise: self.enterpriseName) { result in
                    
                }
            }
        } else {
            self.pushkitToken = pToken
            self.normalToken = nToken
        }
    }
    
    func handlePushMessage(payload data : [AnyHashable : Any]) {
        if CallCenterSDK.isSdkLoggedIn() == false {
            return
        }
        
        var dataString = data["data"] as? String ?? ""
        if dataString.count == 0 {
            let aps = data["aps"] as? [String : Any] ?? [:]
            dataString = aps["alert"] as? String ?? ""
        }
        
        CallCenterSDK.handlePushkitPayload(dataString)
    }
}
