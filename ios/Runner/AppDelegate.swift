import UIKit
import Flutter
import PushKit
import CCSDK


@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    
    var pushRegistry : PKPushRegistry? = nil

  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
      // register for VOIP Push
      pushRegistry = PKPushRegistry(queue: DispatchQueue.main)
      pushRegistry?.delegate = self
      pushRegistry?.desiredPushTypes = [.voIP]
      
    if let controller = window?.rootViewController as? FlutterViewController {
        QuickomContactCenterChannel.shared.registerFlutterChannels(controller: controller)
    }
    
    GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}


extension AppDelegate : PKPushRegistryDelegate {
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        
        if pushCredentials.token.count == 0 {
            NSLog("Voip push token is nil")
            return
        }
        
        let token = pushCredentials.token.map { String(format: "%02.2hhx", $0) }.joined()
        QuickomContactCenterChannel.shared.handleReceivePushToken(pushkitToken: token, normalToken: nil)
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
        
        QuickomContactCenterChannel.shared.handlePushMessage(payload: payload.dictionaryPayload)
    }
}

