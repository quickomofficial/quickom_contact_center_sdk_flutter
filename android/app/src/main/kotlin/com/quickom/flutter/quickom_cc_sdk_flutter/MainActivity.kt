package com.quickom.flutter.quickom_cc_sdk_flutter

import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import androidx.annotation.NonNull

class MainActivity: FlutterActivity() {

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)

        QuickomContactCenterChannel.registerFlutterChannel(flutterEngine, context as MainActivity)
    }

}
