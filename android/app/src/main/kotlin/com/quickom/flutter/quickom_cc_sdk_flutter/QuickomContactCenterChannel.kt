package com.quickom.flutter.quickom_cc_sdk_flutter

import android.app.Activity
import android.util.Log
import androidx.annotation.NonNull
import com.beowulfchain.quickom.callcentersdk.CallCenterSdk
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import android.provider.Settings



object QuickomContactCenterChannel {

    private val CHANNEL = "com.quickom.sdk.contact_center"

    private var methodChannel : MethodChannel? = null
    private var currentContext : Activity? = null

    private var enterpriseName : String? = null

    val fmcMethodNameStart = "start"
    val fmcMethodNameForceRequestCall = "force_request_call"

    val fmcForceRequestCallReturnResult = "force_request_call_return_result"
    val fmcForceRequestCallReceiveInformation = "force_request_call_receive_info"
    val fmcLoginResult = "login"

    val fmcMethodNameLogin = "login"
    val fmcMethodNameLogout = "logout"
    val fmcMethodNameUpdatePushToken = "update_push_token"
    val fmcMethodNameHandlePushMessage = "handle_push_message"

    var pushToken : String? = null


    private val onCallListener = object: CallCenterSdk.CallCenterSdkListener {
        override fun onSuccess() {
            currentContext?.runOnUiThread {
                var info = mapOf<String, Any>(
                        "success" to 1
                )

                Log.d("onCallListener", "onSuccess: sss")

                methodChannel?.invokeMethod(fmcForceRequestCallReturnResult, info)
            }
        }

        override fun onError(errorCode: Int, errorDescription: String) {
            currentContext?.runOnUiThread {
                var info = mapOf(
                        "success" to 0,
                        "errorCode" to errorCode,
                        "errorDescription" to errorDescription
                )

                methodChannel?.invokeMethod(fmcForceRequestCallReturnResult, info)
            }
        }
    }

    private val onLoginListener = object: CallCenterSdk.CallCenterSdkListener {
        override fun onSuccess() {
            currentContext?.runOnUiThread {
                var info = mapOf<String, Any>(
                    "success" to "1"
                )

                Log.d("onLoginListener", "onSuccess: login success")

                if (pushToken != null) {
                    val deviceId = Settings.Secure.getString(currentContext?.contentResolver, Settings.Secure.ANDROID_ID)
                    if (deviceId != null && pushToken != null) {
                        CallCenterSdk.updatePushToken(deviceId, enterpriseName, pushToken)
                    }
                }

                methodChannel?.invokeMethod(fmcLoginResult, info)
            }
        }

        override fun onError(errorCode: Int, errorDescription: String) {
            currentContext?.runOnUiThread {
                var info = mapOf(
                        "success" to 0,
                        "errorCode" to errorCode,
                        "errorDescription" to errorDescription
                )

                methodChannel?.invokeMethod(fmcLoginResult, info)
            }
        }
    }


    fun registerFlutterChannel(@NonNull flutterEngine: FlutterEngine, @NonNull context: Activity) {
        currentContext = context

        methodChannel = MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL)
        methodChannel?.setMethodCallHandler { call, result ->
            if (call.method == fmcMethodNameStart) {
                var args = call.arguments as? Map<String, String>
                if (args != null) {
                    enterpriseName = args["enterpriseName"]
                }
                CallCenterSdk.init(context, "com.quickom.flutter.quickom_cc_sdk_flutter.fileprovider", "vi")
            } else if (call.method == fmcMethodNameForceRequestCall) {
                var args = call.arguments as? Map<String, String>
                if (args != null) {

                    var url = args["url"]
                    if (url != null) {
                        var name = args["name"]
                        var email = args["email"]
                        var phone = args["phone"]
                        var claimCode = args["claimCode"]
                        var option = (args["option"] as? Int) ?: CallCenterSdk.CallOption.NORMAL.value

                        CallCenterSdk.start(url, name, email, phone, onCallListener, option)
                    }
                }
            } else if (call.method == fmcMethodNameLogin) {
                var args = call.arguments as? Map<String, String>
                if (args != null) {

                    var username = args["username"]
                    var pass = args["password"]
                    if (username != null && pass != null) {
                        CallCenterSdk.login(username, pass, onLoginListener)
                    }
                }
            } else if (call.method == fmcMethodNameLogout) {
                var args = call.arguments as? Map<String, String>
                if (args != null) {
                    val deviceId = Settings.Secure.getString(currentContext?.contentResolver, Settings.Secure.ANDROID_ID)
                    if (deviceId != null) {
                        CallCenterSdk.logOut(deviceId)
                    } else {
                        CallCenterSdk.logOut("")
                    }
                }
            } else if (call.method == fmcMethodNameUpdatePushToken) {
                var args = call.arguments as? Map<String, String>
                if (args != null) {
                    val deviceId = Settings.Secure.getString(currentContext?.contentResolver, Settings.Secure.ANDROID_ID)
                    var pushToken = args["pushToken"]
                    if (deviceId != null && pushToken != null && enterpriseName != null) {
                        CallCenterSdk.updatePushToken(deviceId, enterpriseName, pushToken)
                    }
                }
            } else if (call.method == fmcMethodNameHandlePushMessage) {
                var args = call.arguments as? Map<String, String>
                if (args != null) {
                    val deviceId = Settings.Secure.getString(currentContext?.contentResolver, Settings.Secure.ANDROID_ID)
                    var data = (args["data"] as? Map<String, String>) ?: mapOf<String, String>()

                    CallCenterSdk.handlePushMessage(deviceId, data)
                }
            }
        }
    }
}