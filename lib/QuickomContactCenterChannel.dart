import 'package:flutter/services.dart';

typedef FunctionQuickomContactCenterReceiveOldChatEntryCode = Function(String claimCode, {String email, String phone});
typedef FunctionQuickomContactCenterRequestCallResult = Function(bool isSuccessful, {QuickomContactCenterError? error});
typedef FunctionQuickomContactCenterLoginResult = Function(bool isSuccessful, {QuickomContactCenterError? error});
typedef FunctionQuickomContactCenterLogoutResult = Function(bool isSuccessful, {QuickomContactCenterError? error});

class QuickomContactCenterError {

  String? errorCode;
  String? errorDescription;

  QuickomContactCenterError(this.errorCode, {this.errorDescription});
  QuickomContactCenterError.fromJson(Map<String, dynamic> json) {
    errorCode = json["errorCode"];
    errorDescription = json["errorDescription"];
  }
  String get debugDescription {
    return '${this.errorCode} - description: ${this.errorDescription}';
  }


}

class QuickomContactCenterChannel {

  static final shared = QuickomContactCenterChannel();

  static const _platform = const MethodChannel('com.quickom.sdk.contact_center');


  static final String fmcMethodNameStart = "start";
  static final String fmcMethodNameForceRequestCall = "force_request_call";
  static final String fmcMethodNameLogin = "login";
  static final String fmcMethodNameLogout = "logout";
  static final String fmcMethodNameUpdatePushToken = "update_push_token";
  static final String fmcMethodNameHandlePushMessage = "handle_push_message";

  //Callback result
  static final String fmcForceRequestCallReturnResult = "force_request_call_return_result";
  static final String fmcForceRequestCallReceiveInformation = "force_request_call_receive_info";
  static final String fmcLoginResult = "login";


  FunctionQuickomContactCenterReceiveOldChatEntryCode? _fnOldChatEntryCode;
  FunctionQuickomContactCenterRequestCallResult? _fnCallRequestResult;

  FunctionQuickomContactCenterLoginResult? _fnLogin;

  QuickomContactCenterChannel() {
    _setupListenerFromNative();
  }

  Future<void> invokeNativePlatform(String methodName, {Map<String, dynamic>? args}) async {
    if (args != null) {
      await _platform.invokeMethod(methodName, args);
    } else {
      await _platform.invokeMethod(methodName);
    }
  }

  void _setupListenerFromNative() {
    _platform.setMethodCallHandler( (call) {
      if (call.method == fmcForceRequestCallReturnResult) {
        if (call.arguments != null) {
          var success = call.arguments["success"] as int?;
          if (success != null) {
            if (success == 1) {
              if (_fnCallRequestResult != null) {
                _fnCallRequestResult!(true);
              }
            } else {
              var errorCode = call.arguments["errorCode"]?.toString();
              var errorDesc = call.arguments["errorDescription"];
              if (errorCode != null) {
                if (_fnCallRequestResult != null) {
                  var error = QuickomContactCenterError(errorCode, errorDescription: errorDesc);
                  _fnCallRequestResult!(false, error: error);
                }
              } else {
                if (_fnCallRequestResult != null) {
                  var error = QuickomContactCenterError('-1', errorDescription: 'unknown_error');
                  _fnCallRequestResult!(false, error: error);
                }
              }
            }
          }
        }

        return Future.value("");
      } else if (call.method == fmcForceRequestCallReceiveInformation) {
        if (call.arguments != null) {
          var claimCode = call.arguments["code"] as String?;
          if (claimCode != null) {
            var email = call.arguments["email"];
            var phone = call.arguments["phone"];

            //Save claimCode here with email or phone
            if (_fnOldChatEntryCode != null) {
              _fnOldChatEntryCode!(claimCode, email: email, phone: phone);
            }
          }
        }

        return Future.value("");
      } else if (call.method == fmcLoginResult) {
        if (call.arguments != null) {
          var success = call.arguments["success"] as String?;
          if (success != null) {
            if (_fnLogin != null) {
              if (success == "1") {
                _fnLogin!(true);
              } else {
                _fnLogin!(false, error: QuickomContactCenterError('-1', errorDescription: "login failed"));
              }
            } else {
              _fnLogin!(false, error: QuickomContactCenterError.fromJson(call.arguments));
            }
          } else {
            _fnLogin!(false, error: QuickomContactCenterError.fromJson(call.arguments));
          }
        }

        return Future.value("");
      } else {
        print('Unknowm method ${call.method}');
        throw MissingPluginException();
      }
    });
  }

  QuickomContactCenterError? startSDK({required String enterpriseName}) {
    if (enterpriseName.isEmpty) {
      return QuickomContactCenterError('00001', errorDescription: 'Invalid enterprise name');
    }
    invokeNativePlatform(fmcMethodNameStart, args: {'enterpriseName' : enterpriseName});

    return null;
  }

  forceCallContactCenter({
    required String url,
    String? name,
    String? email,
    String? phone,
    String? claimCode,
    FunctionQuickomContactCenterReceiveOldChatEntryCode? fnReceiveOldChatEntryCode,
    FunctionQuickomContactCenterRequestCallResult? fnRequestCallResult
  }) {
    var info = {'url' : url};

    if (name != null && name.isNotEmpty) info['name'] = name;
    if (email != null && email.isNotEmpty) info['email'] = email;
    if (phone != null && phone.isNotEmpty) info['phone'] = phone;
    if (claimCode != null) info['claimCode'] = claimCode;

    _fnOldChatEntryCode = fnReceiveOldChatEntryCode;
    _fnCallRequestResult = fnRequestCallResult;

    QuickomContactCenterChannel.shared.invokeNativePlatform(fmcMethodNameForceRequestCall, args: info);
  }

  login({required String username, required String password, required FunctionQuickomContactCenterLoginResult fnLoginResult}) {
    var info = {
      'username' : username,
      'password' : password,
    };

    _fnLogin = fnLoginResult;
    QuickomContactCenterChannel.shared.invokeNativePlatform(fmcMethodNameLogin, args: info);
  }

  logout() {
    QuickomContactCenterChannel.shared.invokeNativePlatform(fmcMethodNameLogout);
  }

  updatePushToken({required String pushToken}) {
    var info = {
      'pushToken' : pushToken,
    };
    QuickomContactCenterChannel.shared.invokeNativePlatform(fmcMethodNameUpdatePushToken, args: info);
  }

  handlePushMessage({Map<String, dynamic>? data}) {
    var info = {
      'data' : data,
    };
    QuickomContactCenterChannel.shared.invokeNativePlatform(fmcMethodNameHandlePushMessage, args: info);
  }
}