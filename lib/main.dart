import 'dart:developer';
import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

import 'QuickomContactCenterChannel.dart';

void main() {
  runApp(MyApp());

  QuickomContactCenterChannel.shared.startSDK(enterpriseName: "quickom_sdk_app");
}


class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Quickom Contact Center SDK Example'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String? title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  TextEditingController urlController = TextEditingController();

  TextEditingController customerIdController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();

  TextEditingController usernameController = TextEditingController();
  TextEditingController passController = TextEditingController();

  var isLogin = false;

  @override
  Widget build(BuildContext context) {

    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title ?? ""),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          children: [
            SizedBox(
              height: 30,
            ),

            Padding(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: Container(
                height: 50,
                width: double.infinity,
                child: Row(
                  children: [
                    Expanded(
                      child: _textformField('Username', usernameController..text = "thai.production.quickomcc.1@mailinator.com"),
                    ),

                    SizedBox(width: 20,),

                    Expanded(
                      child: _textformField('Password', passController..text = "123456"),
                    ),
                  ],
                ),
              ),
            ),

            SizedBox(
              height: 10,
            ),

            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: TextButton(
                onPressed: () => isLogin ? _signOut() : _signIn(),
                child: Container(
                  width: double.infinity,
                  height: 40,
                  color: Colors.lightBlue,
                  child: Center(
                    child: Text(
                      isLogin ? 'Logout' : 'Sign in to receive call',
                      style: TextStyle(
                        color: Colors.white
                      ),
                    ),
                  ),
                )
              ),
            ),


            SizedBox(
              height: 40,
            ),

            Padding(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: _textformField('Enter Quickom contact center URL', urlController),
            ),

            SizedBox(height: 10,),

            Padding(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: _textformField('Enter Customer\'s name (Optional)', nameController),
            ),

            SizedBox(height: 10,),

            Padding(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: _textformField('Enter Customer\'s email (Optional)', emailController),
            ),

            SizedBox(height: 10,),

            Padding(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: _textformField('Enter Customer\'s phone (Optional)', phoneController),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _forceCall,
        tooltip: 'Force Call',
        child: Icon(Icons.call),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  _textformField(String hint, TextEditingController editingController) {
    return Container(
      height: 46,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4.0),
          border: Border.all(color: Colors.black)
      ),
      child: TextFormField(
        keyboardType: TextInputType.text,
        controller: editingController,
        decoration: InputDecoration(
          // isDense: true,
            contentPadding: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
            hintText: hint,
            border: InputBorder.none
        ),
        textAlign: TextAlign.start,
      ),
    );
  }

  _forceCall() {
    if (urlController.text == null || urlController.text.isEmpty) {
      log('Please enter Quickom Contact Center URL');

      return;
    }

    QuickomContactCenterChannel.shared.forceCallContactCenter(
        url: urlController.text,
        name: nameController.text,
        email: emailController.text,
        phone: phoneController.text,
        fnReceiveOldChatEntryCode: (code, {String? email, String? phone}) {
          log('fnReceiveOldChatEntryCode: code=$code, email=$email, phone = $phone');
        },
        fnRequestCallResult: (isSuccessful, {QuickomContactCenterError? error}) {
          if (isSuccessful) {
            log('fnRequestCallResult: call is successful');
          } else {
            log('fnRequestCallResult: call is failed, error=${error?.errorCode}, errorDescription=${error?.errorDescription}');
          }
        }
    );
  }

  _signIn() {
    QuickomContactCenterChannel.shared.login(
      username: usernameController.text,
      password: passController.text,
      fnLoginResult: (isSuccessful, {error}) {
        if (isSuccessful) {
          //ONLY FOR ANDROID
          _configurePushToken();

          setState(() { isLogin = true; });
        } else {
          log('Failed to login with error = ${error?.debugDescription}');
        }
      }
    );
  }

  _signOut() {
    QuickomContactCenterChannel.shared.logout();
  }

  _configurePushToken() async {
    await Firebase.initializeApp();

    try {
      if (Platform.isAndroid) {
        FirebaseMessaging.instance.getToken().then((value) {
          log('Receive android token = $value');

          if (value != null) {
            QuickomContactCenterChannel.shared.updatePushToken(pushToken: value);
          }
        });

        FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
          log('A new onMessageOpenedApp event was published!');
          log('Message data: ${message.data.toString()}');

          QuickomContactCenterChannel.shared.handlePushMessage(data: message.data);
        });

        FirebaseMessaging.onMessage.listen((RemoteMessage message) {
          log('Got a message whilst in the foreground!');
          log('Message data: ${message.data}');

          QuickomContactCenterChannel.shared.handlePushMessage(data: message.data);
        });
      }
    } catch (e) {
      log("");
    }
  }
}
