# Quickom Contact Center SDK for Flutter

Quickom Contact Center SDK - a QRCode-based contact center - for Flutter app

## Integrating Overview
  * Step 1: Integrate iOS Quickom Contact Center SDK to iOS native project which belongs to flutter app project
  * Step 2: Integrate Android Quickom Contact Center SDK to android native project which belongs to flutter app project
  * Step 3: Create a message channel for communication between native (iOS and android) and flutter

## Integrate iOS Quickom Contact Center SDK to iOS native project

  * Step 1: Open iOS project which is placed inside iOS folder of flutter project

  * Step 2: Follow this step-by-step guide on section __INSTALLATION__ to integrate iOS Quickom Contact Center to iOS project

    `https://gitlab.com/quickomofficial/quickom_contact_center_ios`

## Integrate Android Quickom Contact Center SDK to android native project

  * Step 1: Open android project which is placed inside iOS folder of flutter project

  * Step 2: Follow this step-by-step guide on section __INSTALLATION__ to integrate Android Quickom Contact Center to android project
    * __Note__: Also update AndroidManifest.xml as Step 1 in section __SETUP__

    `https://gitlab.com/quickomofficial/quickom_contact_center_sdk_android`


## Communication between native and flutter

### iOS project

  * __Step 1__: Add file `QuickomContactCenterChannel.swift` to iOS project

  * __Step 2__: Open `Runner.xcworkspace`, go to Project / Signing & Capabilities , add `Push Notification` capability

  * __Step 3__: Open file `AppDelegate.swift`, edit the delegate function `application(_ application:didFinishLaunchingWithOptions) -> Bool`

    ```swift
      var pushRegistry : PKPushRegistry? = nil

      override func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        /* 1: Register push kit */
        pushRegistry = PKPushRegistry(queue: DispatchQueue.main)
        pushRegistry?.delegate = self
        pushRegistry?.desiredPushTypes = [.voIP]
          
        /* 2: Create channel for communicating between ios and flutter */
        if let controller = window?.rootViewController as? FlutterViewController {
            QuickomContactCenterChannel.shared.registerFlutterChannels(controller: controller)
        }
        
        GeneratedPluginRegistrant.register(with: self)
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
      }
    ```

  * __Step 4__: Also stay at the file `AppDelegate.swift`, add 2 callback delegate of push kit framework

    ```swift
      func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
          
        if pushCredentials.token.count == 0 {
            NSLog("Voip push token is nil")
            return
        }
        
        let token = pushCredentials.token.map { String(format: "%02.2hhx", $0) }.joined()
        QuickomContactCenterChannel.shared.handleReceivePushToken(pushkitToken: token, normalToken: nil)
      }
      
      func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
          
        QuickomContactCenterChannel.shared.handlePushMessage(payload: payload.dictionaryPayload)
      }
    ```


### Android project

  * __Step 1__: Add file `QuickomContactCenterChannel` to android project

  * __Step 2__: Open file `QuickomContactCenterChannel`, change the declaration `package` to your package name

  * __Step 3__: Open file `MainActivity`, add following overrided function

    ```kotlin
      override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)

        QuickomContactCenterChannel.registerFlutterChannel(flutterEngine, context as MainActivity)
      }
    ```

    * Note: Import these package to avoid compile error

        ```kotlin
            import androidx.annotation.NonNull
            import io.flutter.embedding.engine.FlutterEngine
        ```

### Flutter project

  * __Step 1__: Add file `QuickomContactCenterChannel.dart` to folder `lib` of flutter project

  * __Step 2__: Open file `main.dart`, go to function `main()` add following lines of code

    ```dart
      QuickomContactCenterChannel.shared.startSDK({@required String enterpriseName});
    ```

    + Parameters:

    |Field|Type|Required|Description|
    |----------|------------|------------|------------|
    |`enterpriseName`|`String`|Required|Contact us to get your unique enterprise name|
   

  #### Make call to contact center: 
    * Request contact center call by calling following member function of class `QuickomContactCenterChannel`

    ```dart
      forceCallContactCenter({
        @required String url,
        String name, 
        String email, 
        String phone,
        String claimCode,
        FunctionQuickomContactCenterReceiveOldChatEntryCode fnReceiveOldChatEntryCode,
        FunctionQuickomContactCenterRequestCallResult fnRequestCallResult
      })
    ```

    + Parameters:

    |Field|Type|Required|Description|
    |----------|------------|------------|------------|
    |`url`|`String`|Required|quickom contact center QR Code url|
    |`name`|`String`|Optional|customer's name|
    |`email`|`String`|Optional|customer's email|
    |`phone`|`String`|Optional|customer's phone|
    |`claimCode`|`String`|Optional|claim code to receive old chat|
    |`fnReceiveOldChatEntryCode`|`FunctionQuickomContactCenterReceiveOldChatEntryCode`|Optional|callback return chat code. Later the same customer can use this code to claim old chats|
    |`fnRequestCallResult`|`FunctionQuickomContactCenterRequestCallResult`|Optional|result of the call|
    
  #### Receive call from contact center

    * __Step 1__: Login with Quickom Contact center username and password

      ```dart
        QuickomContactCenterChannel.shared.login({
          @required String username, 
          @required String password, 
          @required FunctionQuickomContactCenterLoginResult fnLoginResult
        })
      ```

      + Parameters:

      |Field|Type|Required|Description|
      |----------|------------|------------|------------|
      |`username`|`String`|Required|Quickom contact member username|
      |`password`|`String`|Required|Quickom contact member password|
      |`fnLoginResult`|`FunctionQuickomContactCenterLoginResult`|Callback with login result|


    * __Step 2__: Update device push token (only needed on Android platform)

      ```dart
        updatePushToken({@required String pushToken})
      ```

      + Parameters:

      |Field|Type|Required|Description|
      |----------|------------|------------|------------|
      |`pushToken`|`String`|Required|Device's push token|

      + Discussion:
        - This function is only effected once Quickom account is successfully authorized on __Step 1__
        - You can get device's push token from Firebase and pass it to SDK via this method


    * __Step 3__: Handle push message (only needed on Android platform)

      ```dart
        handlePushMessage({Map<String, dynamic> data})
      ```

      + Parameters:

      |Field|Type|Required|Description|
      |----------|------------|------------|------------|
      |`data`|`Map<String, dynamic>`|Optional|Pushed message data|

      + Discussion:
        - This function is only effected once Quickom account is successfully authorized on __Step 1__
        - You can get receive push message data from Firebase and pass it to SDK via this method


### Error code

  |Error code|Description|
  |----------|------------|
  |`00001`|Invalid enterprise name|
  |`10100`|Url is invalid|
  |`10102`|Email is invalid|
  |`10103`|Phone number is invalid|
  |`20101`|Qr code not allow call|
  |`20102`|Qr code not allow chat|
  |`30100`|Account is not activated|
  |`30101`|Account not found|
  |`30102`|Wrong user name or password|
  |`-1`|Unknown error|